package br.com.itau.cliente.customer.controller;

import br.com.itau.cliente.customer.models.Customer;
import br.com.itau.cliente.customer.models.dto.CreateCustomerRequest;

import br.com.itau.cliente.customer.models.dto.CustomerMapper;
import br.com.itau.cliente.customer.security.Usuario;
import br.com.itau.cliente.customer.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cliente")
public class CustomerController {

    @Autowired
    private CustomerService service;

    @Autowired
    private CustomerMapper mapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Customer create(@RequestBody CreateCustomerRequest createCustomerRequest) {
        Customer customer = mapper.toCustomer(createCustomerRequest);

        customer = service.create(customer);

        return customer;
    }

    @GetMapping("/{id}")
    public Customer getById(@PathVariable Long id) {
        return service.getById(id);
    }

}
