package br.com.itau.cliente.customer.repository;

import br.com.itau.cliente.customer.models.Customer;
import org.springframework.data.repository.CrudRepository;

public interface CustomerRepository extends CrudRepository<Customer, Long> {
}
