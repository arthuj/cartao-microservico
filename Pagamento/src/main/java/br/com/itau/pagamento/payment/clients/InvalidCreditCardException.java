package br.com.itau.pagamento.payment.clients;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Cartão informado inválido")
public class InvalidCreditCardException extends RuntimeException {
}
