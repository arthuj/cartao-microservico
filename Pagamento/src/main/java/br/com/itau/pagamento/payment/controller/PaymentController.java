package br.com.itau.pagamento.payment.controller;

import br.com.itau.pagamento.payment.models.Payment;
import br.com.itau.pagamento.payment.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PaymentController {

    @Autowired
    private PaymentService paymentService;

    @PostMapping("/pagamento")
    @ResponseStatus(HttpStatus.CREATED)
    public Payment create(@RequestBody Payment payment) {
        return paymentService.create(payment);
    }

    @GetMapping("/pagamentos/{creditCardId}")
    public List<Payment> findAllByCreditCard(@PathVariable Long creditCardId) {
        return paymentService.findAllByCreditCard(creditCardId);
    }

}
