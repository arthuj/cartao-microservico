package br.com.itau.pagamento.payment.clients;

import feign.Response;
import feign.codec.ErrorDecoder;

public class CreditCardDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder;

    @Override
    public Exception decode(String s, Response response) {
        if(response.status() == 404){
            throw new InvalidCreditCardException();
        }
        else{
            return errorDecoder.decode(s,response);
        }

    }
}
