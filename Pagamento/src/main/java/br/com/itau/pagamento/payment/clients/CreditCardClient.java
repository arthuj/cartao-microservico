package br.com.itau.pagamento.payment.clients;

import br.com.itau.pagamento.payment.models.CreditCard;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cartao", configuration = CreditCardClientConfiguration.class)
public interface CreditCardClient {

    @GetMapping("/cartao/id/{id}")
    CreditCard getCreditCardById(@PathVariable long id);

//    @Modifying
//    @Transactional
//    @Query(value = "delete from pagamento where cartao_id = :idCartao", nativeQuery = true)
//    void deleteCartaoId(@Param("idCartao") int idCartao);
}
