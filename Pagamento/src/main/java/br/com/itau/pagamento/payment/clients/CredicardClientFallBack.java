package br.com.itau.pagamento.payment.clients;

import br.com.itau.pagamento.payment.models.CreditCard;

public class CredicardClientFallBack implements CreditCardClient{

    @Override
    public CreditCard getCreditCardById(long id) {
        throw new CreditCardOfflineException();
    }
}
