package br.com.itau.pagamento.payment.clients;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_GATEWAY, reason = "Sistema de Cartao esta indisponivel")
public class CreditCardOfflineException extends RuntimeException {
}
