package br.com.itau.pagamento.payment.service;

import br.com.itau.pagamento.payment.clients.CreditCardClient;
import br.com.itau.pagamento.payment.models.CreditCard;
import br.com.itau.pagamento.payment.models.Payment;
import br.com.itau.pagamento.payment.repository.PaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaymentService {

    @Autowired
    private PaymentRepository paymentRepository;

    @Autowired
    private CreditCardClient creditCardClient;


    public Payment create(Payment payment) {
        CreditCard creditCard = creditCardClient.getCreditCardById(payment.getCreditCardId());

        payment.setCreditCardId(creditCard.getId());

        return paymentRepository.save(payment);
    }

    public List<Payment> findAllByCreditCard(Long creditCardId) {
        return paymentRepository.findAllByCreditCardId(creditCardId);
    }

}
