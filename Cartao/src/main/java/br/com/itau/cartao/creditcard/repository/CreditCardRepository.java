package br.com.itau.cartao.creditcard.repository;


import br.com.itau.cartao.creditcard.model.CreditCard;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CreditCardRepository extends CrudRepository<CreditCard, Long> {

    Optional<CreditCard> findByNumber(String number);

}
