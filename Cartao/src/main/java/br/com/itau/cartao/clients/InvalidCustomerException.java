package br.com.itau.cartao.clients;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Cliente informado invalido")
public class InvalidCustomerException extends RuntimeException {
}
