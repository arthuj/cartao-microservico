package br.com.itau.cartao.clients;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_GATEWAY, reason = "O sistema de Cliente está indisponível")
public class CustomerOfflineException extends RuntimeException{
}
