package br.com.itau.cartao.clients;

import br.com.itau.cartao.creditcard.model.Customer;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cliente", configuration = CustomerClientConfiguration.class)
public interface CustomerClient {

    @GetMapping("/cliente/{id}")
    Customer getCustomerById(@PathVariable long id);
}
