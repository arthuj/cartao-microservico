package br.com.itau.cartao.clients;

import br.com.itau.cartao.creditcard.model.Customer;

public class CustomerClientFallBack implements CustomerClient{

    @Override
    public Customer getCustomerById(long id) {
        throw new CustomerOfflineException();
    }
}
