package br.com.itau.cartao.clients;

import br.com.itau.cartao.creditcard.exceptions.CreditCardNotFoundException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class CustomerClientDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder;

    @Override
    public Exception decode(String s, Response response) {
        if(response.status() == 404){
            throw new InvalidCustomerException();
        }
        else
            return errorDecoder.decode(s,response);
    }
}
